package oak.forge.domain.event;

import lombok.Getter;
import lombok.Setter;
import oak.forge.commons.data.message.GenericError;

import java.util.UUID;

@Getter
@Setter
public class ClientGenericError extends GenericError {

    private String message;

    public ClientGenericError(String message) {
        super();
        this.message = message;
    }

    public ClientGenericError(UUID receiverId, String message) {
        super(receiverId);
        this.message = message;
    }
}
