package oak.forge.domain.event;

import lombok.Getter;
import oak.forge.commons.data.message.Event;
import oak.forge.domain.model.GameState;

import java.io.Serializable;
import java.util.List;
import java.util.UUID;

@Getter
public class GameSessionCreatedEvent extends Event implements Serializable {

    private final Integer maxNumberOfPlayers;
    private final List<UUID> playerList;
    private final String gameSessionName;
    private final GameState gameState;

    public GameSessionCreatedEvent(Integer maxNumberOfPlayers, List<UUID> playerList, String gameSessionName,
                                   UUID aggregateId, UUID creatorID) {
        super(aggregateId,creatorID);
        this.maxNumberOfPlayers = maxNumberOfPlayers;
        this.playerList = playerList;
        this.gameSessionName = gameSessionName;
        this.gameState = GameState.NOT_RUNNING;
    }
}
