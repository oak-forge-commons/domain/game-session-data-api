package oak.forge.domain.event;

import lombok.*;
import oak.forge.commons.data.message.DomainError;

@Getter
@Setter
public class CreateGameSessionFailedError extends DomainError {

    private String message;
    private String gameSessionName;
    private String activeSessionId;

    public CreateGameSessionFailedError(String message, String gameSessionName, String activeSessionId) {
        super();
        this.message = message;
        this.gameSessionName = gameSessionName;
        this.activeSessionId = activeSessionId;
    }
}
