package oak.forge.domain.model;

public enum GameState {
    NOT_RUNNING,
    RUNNING,
    ENDED;

    private GameState() {
    }
}