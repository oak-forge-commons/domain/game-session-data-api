package oak.forge.domain.command;

import lombok.Getter;
import lombok.NoArgsConstructor;
import oak.forge.commons.data.message.Command;

import java.util.UUID;

@Getter
@NoArgsConstructor
public class CreateGameSessionCommand extends Command {

    private int maxNumberOfPlayers;
    private String gameOwnerName;
    private String gameSessionName;

    public CreateGameSessionCommand(UUID playerId, String gameSessionName, int maxNumberOfPlayers, String gameOwnerName) {
        super(null, playerId);
        this.maxNumberOfPlayers = maxNumberOfPlayers;
        this.gameOwnerName = gameOwnerName;
        this.gameSessionName = gameSessionName;
        withName("create-game-session");
    }
}
